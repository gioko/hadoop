package org.myorg;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
	
public class MovieLensJoiner2 {

	public static class RatingsMapper extends Mapper<LongWritable, Text, LongWritable, Text> {

		private LongWritable k = new LongWritable();
		private Text v = new Text();
        private final static String delimiter = ",";
        
        @Override
        protected void map(LongWritable key, Text input, Context context) throws IOException, InterruptedException {
        	// skip header row
        	if(key.get() == 0 && input.toString().contains("userId")) {
        		return;
        	}
        	// tokens be like (userId movieId rating timestamp)
        	String line = input.toString();
        	// split line
    		String tokens[] = line.split(delimiter);
    		Long movieId = Long.parseLong(tokens[1]);
    		String rating = tokens[2].trim();
    		// flush stream (key, value) pair
    		k.set(movieId);
            v.set("rating\t" + rating);
            context.write(k, v);

        }
	}
	public static class MoviesMapper extends Mapper<LongWritable, Text, LongWritable, Text> {

        private LongWritable k = new LongWritable();
        private Text v = new Text();
        private final static String delimiter = ",";
        
        @Override
        protected void map(LongWritable key, Text input, Context context) throws IOException, InterruptedException {
        	// skip header row
        	if(key.get() == 0 && input.toString().contains("movieId")) {
        		return;
        	}
        	String line = input.toString();
        	//  tokens be like (movieId title genres)
    		String tokens[] = line.split(delimiter);
    		if(!tokens[1].contains("\"")) {
    			Long movieId = Long.parseLong(tokens[0].trim());
        		String title = tokens[1].trim();
        		String genre = tokens[2].trim();
        		// set (key, value) pair and flush stream
        		k.set(movieId);
        		v.set("genre\t" + genre + "\t" + "title\t" + title);
        		context.write(k, v);
    		}
    		// tokens be like (movieId "title, title..." genres)
    		else {
    			Long movieId = Long.parseLong(tokens[0].trim());
    			// concat parts of title
    			String title = "";
    			for(int i=1; i<tokens.length-1; i++) {
    				title += tokens[i];
    			}
    			String genre = tokens[tokens.length-1].trim();
        		// set (key, value) pair and flush stream
    			k.set(movieId);
                v.set("genre\t" + genre + "\t" + "title\t" + title);
                context.write(k, v);
    		}
        }
	}

	public static class JoinReducer extends Reducer<LongWritable, Text, LongWritable, Text> {
			
	        @Override
	        protected void reduce(LongWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
	        	boolean rating_flag = false;
	        	boolean genre_flag = false;
	        	String movieName = "";
	            for (Text t : values) 
	            {
	                String[] parts = t.toString().split("\t");
	                // Check for value being rating
	                if (parts[0].contains("rating"))
	                {
	                	// check if rating > 4.0
	                	if(parts[1].equals("4.5") || parts[1].equals("5.0")) {
	                		rating_flag = true;
	                	}
	                }
	                else {
	                    // Check for value being genre
	                    if (parts[0].contains("genre"))
	                    {
	                    	// check if genre contains Comedy
	                    	if(parts[1].contains("Comedy")) {
	                    		genre_flag = true;
		                	}
	                    }
                        // Check for value being title
		                if (parts[2].contains("title"))
		                {
                            movieName = parts[3].trim();
		                }
	                }
	            }
	            if ((rating_flag == true) || (genre_flag == true)) 
	            {
	                // Output pair [movieId,text message]
	                context.write(key, new Text(movieName));
	            }
	        }
	    }
	    
	
	public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
		// jobs
		if (args.length != 3) 
        {
            System.err.println("Need to provide: <ratings input path> <movies input path> <output path>");
            System.exit(-1);
        }
        //New Job
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "MovieLens-Joiner-2");
        job.setJarByClass(MovieLensJoiner2.class);

        //Reducer
        job.setReducerClass(JoinReducer.class);

        //Output
        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(Text.class);

        //Input
        MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, RatingsMapper.class);
        MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, MoviesMapper.class);
        FileOutputFormat.setOutputPath(job, new Path(args[2]));
        
        //Run and exit
        System.exit(job.waitForCompletion(true) ? 0 : 1);
	}

}
