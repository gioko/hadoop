package org.myorg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class MovieLensGrouping {

	public static class AvgMapper 
		extends Mapper<LongWritable, Text, LongWritable, FloatWritable> 
		{
		private final static String delimiter = ",";
		
		public void map(LongWritable key, Text input, Context context) 
				throws IOException, InterruptedException 
		{
			// Skip csv header
			if (key.get() == 0 && input.toString().contains("userId"))
			{
				return;
			}
			
        	List<String> tokens = split(input.toString(), delimiter);
        	long movieId = Long.parseLong(tokens.get(1).trim());
        	float rating = Float.parseFloat(tokens.get(2).trim());
        	
        	LongWritable movieIdKey = new LongWritable(movieId);
        	FloatWritable ratingValue = new FloatWritable(rating);
        	
          	//Output [movieId, rating]
       	    context.write(movieIdKey, ratingValue);
		}
		
		public static List<String> split(String input, String delimiter)
        {
        	List<String> result = new ArrayList<>();
        	StringTokenizer st = new StringTokenizer(input, delimiter);
            while (st.hasMoreTokens()) {
                result.add(st.nextToken());
            }
        	return result;
        }
	}

	public static class AvgReducer
			extends Reducer<LongWritable, FloatWritable, LongWritable, Text>
	{
		@Override
		public void reduce(LongWritable key, Iterable<FloatWritable> values, Context context)
				throws IOException, InterruptedException
		{
			float avgRating = 0;
			int count = 0;
			float max = 0;

			for (FloatWritable value : values)
			{
				// Add up all the ratings
				avgRating += value.get();
				count++;

				//Calculate max
				if (value.get() > max)
				{
					max = value.get();
				}
			}

			// Divide by the number of ratings to get the average for this movie
			avgRating /= count;
			
			// Use of String format to print only the last 2 decimal digits
			String avg = String.valueOf(avgRating);
			int indexOfDecimal = avg.indexOf(".");
			if(avg.substring(indexOfDecimal + 1).length() > 2) {
				avg = String.format("%.2f" , avgRating);
			}

			// Output the average/max ratings for this movie
            Text outputText = new Text("AVG: " + avg +", MAX: " + max);
			context.write(key, outputText);
		}
	}

	public static void main(String[] args) throws Exception {
		if (args.length != 2) {
			System.err.println("Need to provide: input path and output path");
			System.exit(-1);
		}

		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "MovieLens-Grouping");
		job.setJarByClass(MovieLensGrouping.class);

		job.setMapperClass(AvgMapper.class);
		job.setReducerClass(AvgReducer.class);

		// Set output key/value
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(FloatWritable.class);
		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(Text.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		// Run the job and then exit the program
		System.exit(job.waitForCompletion(true) ? 0 : 1);

	}
}
