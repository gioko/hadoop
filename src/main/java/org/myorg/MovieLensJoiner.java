package org.myorg;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class MovieLensJoiner {

	   public static class JoinMapper extends Mapper<LongWritable, Text, LongWritable, PartitionStringWritable> {

	        private LongWritable k = new LongWritable();
	        private PartitionStringWritable v = new PartitionStringWritable();
	        private final static String delimiter = ",";

	        @Override
	        protected void map(LongWritable key, Text input, Context context) throws IOException, InterruptedException {

	        	// skip movies header row
	        	if(key.get() == 0 && input.toString().contains("movieId")) {
	        		return;
	        	}
	        	// skip ratings header row
	        	else if(key.get() == 0 && input.toString().contains("userId")) {
	        		return;
	        	}
	        	else {
	        		// custom split()
	        		List<String> tokens = split(input.toString(), delimiter);
	        		Long movieId;
	        		
	        		// tokens be like (userId movieId rating timestamp)
	        		// rating > 4.0
	        		if(tokens.get(2).equals("4.5") || tokens.get(2).equals("5.0")) {
	        			movieId = Long.parseLong(tokens.get(1));
	        			k.set(movieId);
	        			v.setPartition(Partition.RIGHT);
	        			v.setValue(tokens.get(1));
	        			context.write(k, v);
	        		}
	        		// tokens inside double quotes be like (movieId "title1, title2, ..., title3" genres)
	        		else if(tokens.get(1).startsWith("\"") && tokens.get(tokens.size()-2).endsWith("\"")) {
	        			String title ="";
	        			for(int i=1; i<tokens.size()-1; i++) {
	        				if(i==tokens.size()-2) {
	        					title += tokens.get(i);
	        				}
	        				else {
	        					title += tokens.get(i) + ",";
	        				}
	        			}
	        			tokens.add(title);
	        			movieId = Long.parseLong(tokens.get(0));
	        			k.set(movieId);
	        			v.setPartition(Partition.LEFT);
	        			v.setValue(tokens.get(tokens.size()-1));
	        			context.write(k, v);		
	        		}
	        		// split has no double quotes (movieId title genres) etc. 206409 movieId
	        		else if(tokens.size() == 3) {
	        			movieId = Long.parseLong(tokens.get(0));
	        			k.set(movieId);
	        			v.setPartition(Partition.LEFT);
	        			v.setValue(tokens.get(tokens.size()-2));
	        			context.write(k, v);
	        		}
	        	}
	        }

			private static List<String> split(String input, String delimiter) {
				List<String> tokens = new ArrayList<String>();
				StringTokenizer tokenizer = new StringTokenizer(input, delimiter);
				while(tokenizer.hasMoreTokens()) {
					tokens.add(tokenizer.nextToken());
				}
				return tokens;
			}

	    }

	    public static class JoinReducer extends Reducer<LongWritable, PartitionStringWritable, Text, Text> {
	    	// 2 partitions
	        private final Set<String> R = new LinkedHashSet<>();
	        private final Set<String> L = new LinkedHashSet<>();
	        // key, value exit pair
	        private Text k = new Text();
	        private Text v = new Text();

	        @Override
	        protected void reduce(LongWritable key, Iterable<PartitionStringWritable> values, Context context) throws IOException, InterruptedException {

	            // partition into two sets            
	            R.clear();
	            L.clear();
	            
	            for(PartitionStringWritable x: values) {
	            	if(x.getPartition().equals(Partition.LEFT)) {
	            		L.add(x.getValue());
	            	} else {
	            		R.add(x.getValue());
	            	}
	            	
	            }
	            
	          	for(String r: R) {
            		for(String l: L) {
            			k.set(r);
        				v.set(l);
	            		context.write(k, v);
            		}
            	}
	          
	        }
	    }
	    
	    public enum Partition { 
	        LEFT, 	// etc. Movies table
	        RIGHT	// etc. Ratings table
	    }

	    public static class PartitionStringWritable implements Writable {

	        // which partition? left or right
	        private Partition partition;
	        // value
	        private String value;
	        
	        public PartitionStringWritable() {
			}

	        public PartitionStringWritable(Partition partition, String value) {
				this.partition = partition;
				this.value = value;
			}

			public Partition getPartition() {
				return partition;
			}

			public void setPartition(Partition partition) {
				this.partition = partition;
			}

			public String getValue() {
				return value;
			}

			public void setValue(String value) {
				this.value = value;
			}

			@Override
	        public void readFields(DataInput in) throws IOException {
				if(in.readBoolean()) {
					partition = Partition.LEFT;
				} else {
					partition = Partition.RIGHT;
				}
				value = in.readLine();
	        }

			@Override
	        public void write(DataOutput out) throws IOException {
				out.writeBoolean(partition.equals(Partition.LEFT)?true:false);
				out.writeChars(value);
	        }

			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + ((partition == null) ? 0 : partition.hashCode());
				result = prime * result + ((value == null) ? 0 : value.hashCode());
				return result;
			}

			@Override
			public boolean equals(Object obj) {
				if (this == obj)
					return true;
				if (obj == null)
					return false;
				if (getClass() != obj.getClass())
					return false;
				PartitionStringWritable other = (PartitionStringWritable) obj;
				if (partition != other.partition)
					return false;
				if (value == null) {
					if (other.value != null)
						return false;
				} else if (!value.equals(other.value))
					return false;
				return true;
			}
			
	    }
	
	public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
		Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "MovieLens Joiner");
        job.setJarByClass(MovieLensJoiner.class);
        job.setMapperClass(JoinMapper.class);
        job.setReducerClass(JoinReducer.class);
        job.setMapOutputKeyClass(LongWritable.class);
        job.setMapOutputValueClass(PartitionStringWritable.class);
        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(PartitionStringWritable.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
	}

}
