package org.myorg;

import java.io.IOException;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class MovieIdRating {

    public static class MovieIdRatingMapper
            extends Mapper<LongWritable, Text, LongWritable, FloatWritable> 
    {
    	private final static float floatThreshold = 3.0f;
    	public final static String delimiter = ",";
        
        @Override
        public void map(LongWritable key, Text input, Context context) 
        		throws IOException, InterruptedException 
        {
        	if (key.get() == 0 && input.toString().contains("userId")) 
        	{
        		//Skip csv header
        		return;
        	}
        	List<String> tokens = split(input.toString(), delimiter);
        	long movieId = Long.parseLong(tokens.get(1).trim());
        	float rating = Float.parseFloat(tokens.get(2).trim());
        	
        	LongWritable movieIdKey = new LongWritable(movieId);
        	FloatWritable ratingValue = new FloatWritable(rating);
        	FloatWritable thresholdValue = new FloatWritable(floatThreshold);
        	
        	if (ratingValue.compareTo(thresholdValue) >= 0 )
        	{
            	//Output [movieId, rating]
        	    context.write(movieIdKey, ratingValue);
        	}
        }
        
        public static List<String> split(String input, String delimiter)
        {
        	List<String> result = new ArrayList<>();
        	StringTokenizer st = new StringTokenizer(input, delimiter);
            while (st.hasMoreTokens()) {
                result.add(st.nextToken());
            }
        	return result;
        }
    }

    public static class MovieIdRatingReducer
            extends Reducer<LongWritable, FloatWritable, LongWritable, FloatWritable> 
    {
        @Override
        public void reduce(LongWritable key, Iterable<FloatWritable> values, Context context)
        		throws IOException, InterruptedException 
        {
            // NewList
            List<FloatWritable> newList = new ArrayList<FloatWritable>(); 
            for (FloatWritable val : values) 
            {
                newList.add(val);
            }
            //Remove duplicates
            List<FloatWritable> distinctElements = newList.stream().distinct().collect(Collectors.toList());
            
            for (FloatWritable element : distinctElements) 
            {
                // Output pair [movieId,rating]
            	context.write(key, element);
            }
        }
    }

    public static void main(String[] args) throws Exception 
    {
        if (args.length != 2) 
        {
            System.err.println("Need to provide: input path and output path");
            System.exit(-1);
        }
        //New Job
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "MovieId-Rating");
        job.setJarByClass(MovieIdRating.class);
        
        //Mapper-Combiner-Reducer
        job.setMapperClass(MovieIdRatingMapper.class);
        job.setCombinerClass(MovieIdRatingReducer.class);
        job.setReducerClass(MovieIdRatingReducer.class);
        
        //Output
        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(FloatWritable.class);
        
        //Input
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        
        //Run and exit
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

}
