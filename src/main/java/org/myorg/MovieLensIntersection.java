package org.myorg;

import java.io.IOException;
import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class MovieLensIntersection {

    public static List<String> split(String input, String delimiter)
    {
        List<String> result = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(input, delimiter);
        while (st.hasMoreTokens()) {
            result.add(st.nextToken());
        }
        return result;
    }

    public static class MovieIdRatingMapper
            extends Mapper<LongWritable, Text, LongWritable, Text> 
    {
        private final static float floatThreshold = 3.0f;
        private final static String delimiter = ",";
        
        @Override
        public void map(LongWritable key, Text input, Context context) 
                throws IOException, InterruptedException 
        {
            if (key.get() == 0 && input.toString().contains("userId")) 
            {
                //Skip ratings.csv header
                return;
            }
            List<String> tokens = split(input.toString(), delimiter);
            long movieId = Long.parseLong(tokens.get(1).trim());
            float rating = Float.parseFloat(tokens.get(2).trim());

            LongWritable movieIdKey = new LongWritable(movieId);
            FloatWritable ratingValue = new FloatWritable(rating);
            FloatWritable thresholdValue = new FloatWritable(floatThreshold);

            //Find movies with rating >=3
            if (ratingValue.compareTo(thresholdValue) >= 0 )
            {
                //Output [movieId, rating]
                context.write(movieIdKey, new Text("rating\t" + rating)); //there are still duplicates
            }
        }
    }

    public static class MovieIdGenreMapper
           extends Mapper<LongWritable, Text, LongWritable, Text> 
{
    public final static String delimiter = ",";

    @Override
    public void map(LongWritable key, Text input, Context context) 
             throws IOException, InterruptedException 
    {
        if (key.get() == 0 && input.toString().contains("movieId")) 
        {
            //Skip movies.csv header
            return;
        }
        List<String> tokens = split(input.toString(), delimiter);
        Long movieId = Long.parseLong(tokens.get(0));
        String genres = tokens.get(tokens.size()-1).toString().trim();
        StringUtils.strip(genres);
        if (genres.contains("Comedy") == true)
        {
            LongWritable movieIdKey = new LongWritable(movieId);
            //Output [movieId, genre]
            context.write(movieIdKey, new Text ("genre\t" + "Comedy"));
        }
        
	}
}

    public static class MovieIdRatingGenreReducer
            extends Reducer<LongWritable, Text, LongWritable, Text> 
    {
        @Override
        public void reduce(LongWritable key, Iterable<Text> movie_info, Context context)
                throws IOException, InterruptedException 
        {
            boolean rating_flag = false;
            boolean genre_flag = false;
            for (Text info : movie_info) 
            {
                String[] info_list = info.toString().split("\t");
                // Check for value being rating
                if (info_list[0].contains("rating"))
                {
                    if (rating_flag == false) 
                    {
                        rating_flag = true;
                    }
                }
                else {
                    // Check for value being genre
                    if (info_list[0].contains("genre"))
                    {
                        if (genre_flag == false)
                        {
                            genre_flag = true;
                        }
                    }
                }
            }
            if ((rating_flag == true) && (genre_flag == true)) 
            {
                // Output pair [movieId,text message]
                context.write(key, new Text("High rated Comedy!"));
            }
        }
    }    

    public static void main(String[] args) throws Exception 
    {
        if (args.length != 3) 
        {
            System.err.println("Need to provide: <ratings input path> <movies input path> <output path>");
            System.exit(-1);
        }
        //New Job
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "MovieId-Rating-Genre");
        job.setJarByClass(MovieIdRating.class);

        //Reducer
        job.setReducerClass(MovieIdRatingGenreReducer.class);

        //Output
        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(Text.class);

        //Input
        MultipleInputs.addInputPath(job, new Path(args[0]), TextInputFormat.class, MovieIdRatingMapper.class);
        MultipleInputs.addInputPath(job, new Path(args[1]), TextInputFormat.class, MovieIdGenreMapper.class);
        FileOutputFormat.setOutputPath(job, new Path(args[2]));
        
        //Run and exit
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

}
