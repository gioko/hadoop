/**  How to compile **/

For current project Eclipse was used, with name "hadoop"

1. Import a Maven project in Eclipse, containing pom.xml and java classes

2. Make clean through Run--> Run as--> Maven clean

3. Create a new "hadoop" configuration through Run--> Run Configurations, with Goal: package

4. Build project through Run--> Maven build and select hadoop:package

/** How to run **/

1. Start your Hadoop VM and get into it
$ vagrant up
$ vagrant ssh

As soon as you are into the VM, console will look like "vagrant@ubuntu-bionic:~$"
Then you can continue with the following:

2. Start Hadoop deamons
$ hadoop/sbin/start-dfs.sh
$ hadoop/sbin/start-yarn.sh

3. Create an input folder and place csv files inside
$ hadoop fs -mkdir -p /hadoop/input
$ hadoop fs -put /vagrant/data/hadoop-git-project/input/* /hadoop/input

4. Run the jar file, giving the correct class name and input, output paths

4.1 Selection + Projection query
$ hadoop jar /vagrant/data/hadoop-git-project/hadoop/target/hadoop-0.1.jar org.myorg.MovieIdRating /hadoop/input/ratings.csv /hadoop/output

4.2 Intersection query
$ hadoop jar /vagrant/data/hadoop-git-project/hadoop/target/hadoop-0.1.jar org.myorg.MovieLensIntersection  /hadoop/input/ratings.csv /hadoop/input/movies.csv /hadoop/output

4.3 Inner Join query (1)
hadoop jar /vagrant/data/hadoop-git-project/hadoop/target/hadoop-0.1.jar org.myorg.MovieLensJoiner /hadoop/input /hadoop/output

4.4 Inner Join query (2)
hadoop jar /vagrant/data/hadoop-git-project/hadoop/target/hadoop-0.1.jar org.myorg.MovieLensJoiner2 /hadoop/input/ratings.csv /hadoop/input/movies.csv /hadoop/output

4.5 Grouping + Aggregation query
$ hadoop jar /vagrant/data/hadoop-git-project/hadoop/target/hadoop-0.1.jar org.myorg.MovieLensGrouping /hadoop/input/ratings.csv /hadoop/output

5. Get results locally
$ hadoop fs -get /hadoop/output/* /vagrant/data/hadoop-git-project/output

6. Validate respective output/part-r-xxx file or convert first to .csv format
$ cat part-r-00000 | tr "\t" "," >> outputwithcomma.csv


7. To run again, remove the /hadoop/output folder
$ hadoop fs -rm /hadoop/output/*
$ hadoop fs -rmdir /hadoop/output/
